[![Codeship Status for jrodriguezorjuela/cs413f16p2](https://codeship.com/projects/fe9fa430-60d6-0134-da50-0636ca74ecab/status?branch=master)](https://codeship.com/projects/174664)

# Get TODO from Console
	git grep -nIE "TODO"

# Run a test on a single class/method

	gradle test --tests "*TEST_CLASS.METHOD"

## **TODO** - TestIterator.java

CODE LINE | TASK | COMMENT
------|------|--------
java:24| // TODO also try with a LinkedList - does it make any difference? | All the test run correctly although when using LinkedList some test might take additional time.  
java:50| // TODO fix the expected values in the assertions below | COMPLETED
java:78| // TODO what happens if you use list.remove(77)? | Will get an error as the method remove the index of the element to be remove. In this particular list we dont have an element at index 77.
java:81| // TODO using assertEquals and Arrays.asList, express which values are left in the list | Arrays.asList(33, 44, 55, 66)
java:98| // TODO use an iterator and a while loop to compute the average (mean) of the values (defined as the sum of the items divided by the number of items) | COMPLETED

## **TODO** - TestList.java

CODE LINE | TASK | COMMENT
------|------|------
java:22| // TODO also try with a LinkedList - does it make any difference? | COMPLETED
java:43| // TODO fix the expected values in the assertions below | COMPLETED
java:52| // TODO write assertions using list.contains(77) that hold before and after adding 77 to the list | COMPLETED
java:63| // TODO fix the expected values in the assertions below | COMPLETED
java:79| // TODO fix the expected values in the assertions below | COMPLETED
java:97| // TODO list.remove(5); //what does this method do? | Removes the entry at index[5]
java:98| // TODO fix the expected values in the assertions below | COMPLETED
java:121| // TODO using containsAll and Arrays.asList (see above) | COMPLETED
java:129| // TODO in a single statement using addAll and Arrays.asList | COMPLETED
java:151| // TODO in a single statement using removeAll and Arrays.asList | COMPLETED
java:167| // TODO in a single statement using retainAll and Arrays.asList | COMPLETED
java:183| // TODO use the set method to change specific elements in the list such that the following assertions pass (without touching the assertions themselves) | COMPLETED
java:205| // TODO fix the arguments in the subList method so that the assertion passes | COMPLETED

## **TODO** - TestPerformance.java

CODE LINE | TASK | COMMENT
------|------|------
java:13| // TODO run test and record running times for SIZE = 10, 100, 1000, 10000  | COMPLETED

### Performance Test 

METHOD|SIZE|TOTAL TIME|
----|-----|----------
testLinkedListAddRemove|10|3.044 secs
testArrayListAddRemove|10|2.862 secs
testLinkedListAccess|10|2.883 secs
testArrayListAccess|10|2.724 secs

METHOD|SIZE|TOTAL TIME|
----|-----|----------
testLinkedListAddRemove|100|3.264 secs
testArrayListAddRemove|100|3.106 secs
testLinkedListAccess|100|2.768 secs
testArrayListAccess|100|2.924 secs

METHOD|SIZE|TOTAL TIME|
----|-----|----------
testLinkedListAddRemove|1000|3.114 secs
testArrayListAddRemove|1000|4.812 secs
testLinkedListAccess|1000|3.365 secs
testArrayListAccess|1000|3.42 secs

METHOD|SIZE|TOTAL TIME|
----|-----|----------
testLinkedListAddRemove|10000|3.687 secs
testArrayListAddRemove|10000|19.705 secs
testLinkedListAccess|10000|10.554 secs
testArrayListAccess|10000|2.637 secs
